﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Excepciones;


namespace ModuloPrincipal.CP
{
    public abstract class GestorSqlServer
    {
        protected SqlConnection Conexion;
        private SqlTransaction Transaccion;


        public abstract void AbrirConexion();
        public void CerrarConexion()
        {
            try
            {
                Conexion.Close();
                Conexion.Dispose();
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorCerrarConexion();
            }
        }
        
        public void IniciarTransaccion(string nameTransaction)
        {
            try
            {
               Transaccion = Conexion.BeginTransaction(nameTransaction);
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorIniciarTransaccion();
            }
        }

        public void TerminarTransaccion()
        {
            try
            {
                Transaccion.Commit();
                Conexion.Close();
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorTerminarTransaccion();
            }
        }

        public void CancelarTransaccion()
        {
            try
            {
                Transaccion.Rollback();
                Conexion.Close();
                
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorCancelarTransaccion();
            }
        }

        public SqlCommand PrepararProcedimiento(string nomProcedure)
        {
            SqlCommand sentencia = new SqlCommand();
            sentencia.CommandText = nomProcedure;
            sentencia.CommandType = CommandType.StoredProcedure;
            sentencia.CommandTimeout = 0;
            sentencia.Connection = Conexion;
            if (Transaccion != null)
                sentencia.Transaction = Transaccion;
            return sentencia;
        }
        public SqlCommand PrepararSentencia(string sql)
        {
            SqlCommand sentencia = new SqlCommand(sql, Conexion);
            sentencia.CommandTimeout = 0;
            if (Transaccion != null)
                sentencia.Transaction = Transaccion;          
            return sentencia;
        }
        public SqlDataReader EjecutarConsulta(string sql)
        {
            SqlDataReader resultado;
            SqlCommand sentencia = PrepararSentencia(sql);
            resultado = sentencia.ExecuteReader();
            return resultado;
        }
    }
}
