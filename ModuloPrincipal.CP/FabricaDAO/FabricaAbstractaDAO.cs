﻿using ModuloPrincipal.CD.Base.Contrato;
using ModuloPrincipal.CD.Permisos.Contrato;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CP.FabricaDAO
{
    public abstract class FabricaAbstractaDAO
    {
        public static FabricaAbstractaDAO Instancia() {
            try
            {
                Type claseFabricaDAO = Type.GetType(ConfigurationManager.AppSettings["MOTORBD"].ToString());
                FabricaAbstractaDAO fabricaDAO = (FabricaAbstractaDAO)Activator.CreateInstance(claseFabricaDAO);
                return fabricaDAO;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public abstract GestorSqlServer CrearGestorSqlServer();
        public abstract IMetodoDAO CrearMetodoDAO(GestorSqlServer gestorSql);
        public abstract ISeguridadDAO CrearSeguridadDAO(GestorSqlServer gestorSql);
        public abstract IAuditoriaDAO CrearAuditoriaDAO(GestorSqlServer gestorSql);
    }
}
