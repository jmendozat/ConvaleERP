﻿using ModuloPrincipal.CP.FabricaDAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuloPrincipal.CD.Base.Contrato;
using ModuloPrincipal.CD.Permisos.Contrato;
using ModuloPrincipal.CP.Base;
using ModuloPrincipal.CP.Permisos;

namespace ModuloPrincipal.CP.FabricaDAO
{
    public class ServerLocalHost : FabricaAbstractaDAO
    {
        public override IAuditoriaDAO CrearAuditoriaDAO(GestorSqlServer gestorSql)
        {
            return new AuditoriaDAO(gestorSql);
        }

        public override GestorSqlServer CrearGestorSqlServer()
        {
            return new ConexionSqlServer();
        }

        public override IMetodoDAO CrearMetodoDAO(GestorSqlServer gestorSql)
        {
            return new MetodoDAO(gestorSql);
        }

        public override ISeguridadDAO CrearSeguridadDAO(GestorSqlServer gestorSql)
        {
            return new SeguridadDAO(gestorSql);
        }
    }
}
