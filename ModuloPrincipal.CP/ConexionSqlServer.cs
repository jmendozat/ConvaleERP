﻿using Excepciones;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CP
{
    public class ConexionSqlServer : GestorSqlServer
    {
        public override void AbrirConexion()
        {
            try
            {
                string url = ConfigurationManager.ConnectionStrings["conBDCONVALEERP"].ConnectionString;
                Conexion = new SqlConnection(url);
                Conexion.Open();
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorAbrirConexion();
            }
        }
    }
}
