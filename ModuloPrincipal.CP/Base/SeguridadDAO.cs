﻿using ModuloPrincipal.CD.Base.Contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuloPrincipal.CD.Base.Entidad;
using System.Data.SqlClient;
using Utilitarios.Utils;
using Excepciones;

namespace ModuloPrincipal.CP.Base
{
    public class SeguridadDAO : ISeguridadDAO
    {
        private GestorSqlServer GestorSqlServer;

        public SeguridadDAO(GestorSqlServer GestorSqlServer) {
            this.GestorSqlServer = GestorSqlServer;
        }
        public bool Insertar(Seguridad log)
        {
            try
            {
                SqlCommand sentencia = GestorSqlServer.PrepararProcedimiento("LOGSEGURIDAD_Insert");
                sentencia.Parameters.AddWithValue("@vchMENSAJE", log.Mensaje);
                sentencia.Parameters.AddWithValue("@vchDETALLE", log.Detalle);
                sentencia.Parameters.AddWithValue("@vchIPEQUIPO", log.IPEquipo);
                return sentencia.ExecuteNonQuery() > ParametroSQL.FILAS_AFECTADAS;
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorInsertar();
            }
        }
    }
}
