﻿using ModuloPrincipal.CD.Permisos.Contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuloPrincipal.CD.Permisos.Entidad;
using System.Data.SqlClient;
using Excepciones;
using Utilitarios.Utils;

namespace ModuloPrincipal.CP.Permisos
{
    public class MetodoDAO : IMetodoDAO
    {
        private GestorSqlServer GestorSqlServer;

        public MetodoDAO(GestorSqlServer GestorSqlServer) {
            this.GestorSqlServer = GestorSqlServer;
        }

        public bool Crear(Metodo entidad)
        {
            try
            {
                SqlCommand sentencia = GestorSqlServer.PrepararProcedimiento("METODO_Crear");
                sentencia.Parameters.AddWithValue("@nvcNombre", entidad.Nombre);
                sentencia.Parameters.AddWithValue("@bitIsForPermiso", entidad.IsForPermiso);
                return sentencia.ExecuteNonQuery() > ParametroSQL.FILAS_AFECTADAS;              
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorInsertar();
            }
        }

        public bool Editar(Metodo entidad)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(Metodo entidad)
        {
            throw new NotImplementedException();
        }

        public List<Metodo> Listar()
        {
            try
            {
                List<Metodo> listaMetodos = new List<Metodo>();
                SqlCommand sentencia = GestorSqlServer.PrepararProcedimiento("METODO_Listar");
                SqlDataReader resultado = sentencia.ExecuteReader();
                while (resultado.Read()) {
                    Metodo metodo = new Metodo
                    {
                        Id = Convert.ToInt32(resultado["Id"]),
                        Nombre = resultado["Nombre"].ToString(),
                        IsForPermiso = Convert.ToBoolean(resultado["IsForPermiso"]),
                        Estado = Convert.ToInt32(resultado["Estado"]),
                        FechaCreacion = resultado["FechaCreacion"].ToString(),
                        FechaUltimaModificacion = resultado["FechaUltimaModificacion"].ToString()
                    };
                    listaMetodos.Add(metodo);
                }
                return listaMetodos;
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorConsultar();
            }
        }
    }
}
