﻿using ModuloPrincipal.CD.Permisos.Contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuloPrincipal.CD.Permisos.Entidad;
using Excepciones;
using System.Data.SqlClient;

namespace ModuloPrincipal.CP.Permisos
{
    public class ModuloDAO : IModuloDAO
    {
        private GestorSqlServer GestorSqlServer;
        public ModuloDAO(GestorSqlServer GestorSqlServer) {
            this.GestorSqlServer = GestorSqlServer;
        }
        public bool Crear(Modulo entidad)
        {
            throw new NotImplementedException();
        }

        public bool Editar(Modulo entidad)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(Modulo entidad)
        {
            throw new NotImplementedException();
        }

        public List<Modulo> Listar()
        {
            try
            {
                List<Modulo> listaModulos = new List<Modulo>();
                SqlCommand sentencia = GestorSqlServer.PrepararProcedimiento("MODULO_Listar");
                SqlDataReader resultado = sentencia.ExecuteReader();
                while (resultado.Read())
                {
                    Modulo Modulo = new Modulo
                    {
                        Id = Convert.ToInt32(resultado["Id"]),
                        Nombre = resultado["Nombre"].ToString(),
                        Estado = Convert.ToInt32(resultado["Estado"]),
                        FechaCreacion = resultado["FechaCreacion"].ToString(),
                        FechaUltimaModificacion = resultado["FechaUltimaModificacion"].ToString()
                    };
                    listaModulos.Add(Modulo);
                }
                return listaModulos;
            }
            catch (Exception)
            {
                throw ExcepcionSQL.CrearErrorConsultar();
            }
        }
    }
}
