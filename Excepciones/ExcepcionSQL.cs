﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Excepciones
{
    public class ExcepcionSQL : Exception
    {
        private const string MENSAJE_ERROR_CONSULTAR = "No se pudo realizar la consulta."
                               + "Intente de nuevo o consulte con el Administrador.";
        private const string MENSAJE_ERROR_INSERTAR = "No se pudo guardar los datos."
                + "Verifique los datos obligatorios y \u00FAnicos."
                + "Intente de nuevo o consulte con el Administrador.";
        private const string MENSAJE_ERROR_MODIFICAR = "No se pudo actualizar los datos."
                + "Verifique los datos obligatorios y \u00FAnicos."
                + "Intente de nuevo o consulte con el Administrador.";
        private const string MENSAJE_ERROR_ELIMINAR = "No se pudo eliminar el registro."
                + "Intente de nuevo o consulte con el Administrador.";
        private const string MENSAJE_ERROR_ABRIRCONEXION = "No se pudo abrir la conexi\u00F3n con la base de datos."
                + "Intente de nuevo o consulte con el Administrador.";
        private const string MENSAJE_ERROR_CERRARCONEXION = "No se pudo cerrar la conexi\u00F3n con la base de datos."
                + "Intente de nuevo o consulte con el Administrador.";
        private const string MENSAJE_ERROR_INICIARTRANSACCION = "No se pudo iniciar la transacci\u00F3n."
                + "Intente de nuevo o consulte con el Administrador.";
        private const string MENSAJE_ERROR_TERMINARTRANSACCION = "No se pudo terminar la transacci\u00F3n."
                + "Intente de nuevo o consulte con el Administrador.";
        private const string MENSAJE_ERROR_CANCELARTRANSACCION = "No se pudo cancelar la transacci\u00F3n."
                + "Intente de nuevo o consulte con el Administrador.";   
        public ExcepcionSQL(string message) : base(message) { }
        public static ExcepcionSQL CrearErrorConsultar()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_CONSULTAR);
        }
        public static ExcepcionSQL CrearErrorInsertar()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_INSERTAR);
        }

        public static ExcepcionSQL CrearErrorModificar()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_MODIFICAR);
        }

        public static ExcepcionSQL CrearErrorEliminar()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_ELIMINAR);
        }

        public static ExcepcionSQL CrearErrorAbrirConexion()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_ABRIRCONEXION);
        }

        public static ExcepcionSQL CrearErrorCerrarConexion()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_CERRARCONEXION);
        }

        public static ExcepcionSQL CrearErrorIniciarTransaccion()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_INICIARTRANSACCION);
        }

        public static ExcepcionSQL CrearErrorTerminarTransaccion()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_TERMINARTRANSACCION);
        }

        public static ExcepcionSQL CrearErrorCancelarTransaccion()
        {
            return new ExcepcionSQL(MENSAJE_ERROR_CANCELARTRANSACCION);
        }
        
    }
}
