﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Excepciones
{
    public class ExceptionEmail : Exception
    {
        #region 
        private const string ERROR_DIRECCION_REMITENTE = "No se ha especificado la dirección del remitente.";
        private const string ERROR_DIRECCION_DESTINATARIO = "No se ha especificado la dirección del destinatario.";
        private const string ERROR_CUERPO_MENSAJE = "No se ha especificado el cuerpo del mensaje.";
        private const string ERROR_DIRECCION_EMAIL = "{0} no es una dirección de email válida.";
        private const string ERROR_IMAGEN_NO_DISPONIBLE = "La imagen {0} no se encuentra disponible.";
        private const string ERROR_ADD_IMG_MENSAJE = "Ocurrio un error al intentar incluir el logo en el mensaje.";
        private const string ERROR_ADJUNTAR_ARCHIVOS = "No es posible adjuntar el siguiente archivo: {0}";
        private const string ERROR_ENVIO_MSM_DESTINATARIOS = "No se ha podido enviar el mensaje a todos los destinatarios.";
        private const string ERROR_ERROR_CONEXION_SERVIDOR = "No se ha podido establecer conexión con el servidor de envio de correos.";
        #endregion 

        public ExceptionEmail(string message) : base(message) { }

        public static ExceptionEmail CrearErrorDireccionRemitente()
        {
            return new ExceptionEmail(ERROR_DIRECCION_REMITENTE);
        }

        public static ExceptionEmail CrearErrorDireccionDestinatario()
        {
            return new ExceptionEmail(ERROR_DIRECCION_DESTINATARIO);
        }

        public static ExceptionEmail CrearErrorCuerpoMensaje()
        {
            return new ExceptionEmail(ERROR_CUERPO_MENSAJE);
        }

        public static ExceptionEmail CrearErrorDireccionEmail(string direccionEmail) {
            return new ExceptionEmail(string.Format(ERROR_DIRECCION_EMAIL, direccionEmail));
        }

        public static ExceptionEmail CrearErrorImagenNoDisponible(string parametroImagen)
        {
            return new ExceptionEmail(string.Format(ERROR_IMAGEN_NO_DISPONIBLE, parametroImagen));
        }

        public static ExceptionEmail CrearErrorAddImgMensaje() {
            return new ExceptionEmail(ERROR_ADD_IMG_MENSAJE);
        }

        public static ExceptionEmail CrearErrorAdjuntarArchivo(string nombreArchivo) {
            return new ExceptionEmail(string.Format(ERROR_ADJUNTAR_ARCHIVOS, nombreArchivo));
        }

        public static ExceptionEmail CrearErrorEnvioMsmDestinatario() {
            return new ExceptionEmail(ERROR_ENVIO_MSM_DESTINATARIOS);
        }

        public static ExceptionEmail CrearErrorConexionServidor() {
            return new ExceptionEmail(ERROR_ERROR_CONEXION_SERVIDOR);
        }
    }
}
