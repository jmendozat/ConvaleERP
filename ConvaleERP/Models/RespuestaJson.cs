﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ConvaleERP.Models
{
    public class RespuestaJson<T>
    {
        public string Tipo { get; set; }
        public string Mensaje { get; set; }
        public T Objeto { get; set; }
        public List<T> ListaObjeto { get; set; }
        
    }
}