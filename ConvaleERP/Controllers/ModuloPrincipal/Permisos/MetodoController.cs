﻿using ConvaleERP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ModuloPrincipal.CD.Permisos.Entidad;
using ModuloPrincipal.CA.Servicios.Permisos;
using Utilitarios.Utils;
using Log4netMVC;

namespace ConvaleERP.Controllers.ModuloPrincipal.Permisos
{
    public class MetodoController : Controller
    {
        // GET: Metodo
        public ActionResult Index()
        {
            try
            {
                NetLogger.EscribirLog(NetLogger.NivelLog.Auditoria, "Inicio de Proceso","Se inicio el proceso Index del MetodoController.");
                return View();
            }
            catch (Exception ex)
            {
                NetLogger.EscribirLog(NetLogger.NivelLog.Aplicacion, NetLogger.FormatoExcepciones(ex));
                throw;
            }
        }

        [HttpPost]
        public JsonResult Prueba(Metodo met, string nombre)
        {
            NetLogger.EscribirLog(NetLogger.NivelLog.Auditoria, "Inicio de Proceso", "Se inicio el proceso Prueba del MetodoController.");
            GestionarMetodoServicio.Instance.Crear(met);
            return Json(new RespuestaJson<Metodo> { Tipo = Parametros.TIPO_EXITO,Mensaje= nombre, Objeto=met }, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult Listar() {
            try
            {
                NetLogger.EscribirLog(NetLogger.NivelLog.Auditoria, "Inicio de Proceso", "Se inicio el proceso Listar del MetodoController.");
                List<Metodo> listaMetodo = GestionarMetodoServicio.Instance.Listar();
                if (listaMetodo.Count > 0)
                    return Json(new RespuestaJson<Metodo>
                    {
                        Tipo = Parametros.TIPO_EXITO,
                        Mensaje = "Consulta realizada con exito",
                        ListaObjeto = listaMetodo
                    }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new RespuestaJson<Metodo>{ Tipo = Parametros.TIPO_ALERTA, Mensaje = "Sin registros en la base de datos"}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                NetLogger.EscribirLog(NetLogger.NivelLog.Aplicacion, NetLogger.FormatoExcepciones(ex));
                return Json(new RespuestaJson<Metodo>{Tipo = Parametros.TIPO_ERROR, Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
            }
           

        }
    }
}