﻿$(function () {
    loadPage();
    listar();
});

function loadPage() {
    var nombre = "Manipulacion cliente";
    var metodo = {
        Nombre: "Prueba cliente",
        Estado: 5
    }
    llamadaAJAX("Metodo/Prueba", "{met : " + JSON.stringify(metodo) + ", nombre: '" + nombre + "'}", function (result) {
        console.log(result);
        var metodo = result.Objecto;
        console.log(metodo);
        console.log(metodo.Nombre);
    }, function (xhr, ajaxOptions, thrownError) {
        fn_mensaje("rowMessage", "Ocurrió un error, por favor reintente.", "error");
    });
};

function listar() {
    llamadaAJAX("Metodo/Listar", "{}", function (result) {
        console.log(result);
        if (result.Tipo == "exito") {           
            fn_mensaje("rowMessage", result.Mensaje, result.Tipo);
            $('#contenedor').html(result);
        } else {
            fn_mensaje("rowMessage", result.Mensaje, result.Tipo);
        }          
    }, function (xhr, ajaxOptions, thrownError) {
        fn_mensaje("rowMessage", "Ocurrió un error, por favor reintente.", "error");
    });
};