﻿function fn_setMsj(container, message) {
    var wrap = $('div[id$=' + container + ']');
    wrap.empty();
    wrap.append(message);
    wrap.slideDown();
    $('html, body').animate({ scrollTop: 1 }, 'slow');
    setTimeout(function () {
        wrap.slideUp();
    }, 5000);
}
function fn_mensaje(container, message, type) {
    var close = "";
    var msg;
    switch (type) {
        case "exito":
            msg = "<div class='alert alert-success' id='msgDiv'>" + close + "<strong>Éxito! </strong>" + message + "</div>"
            break;
        case "info":
            msg = "<div class='alert alert-info' id='msgDiv'>" + close + "<strong>Info! </strong>" + message + "</div>"
            break;
        case "alerta":
            msg = "<div class='alert alert-warning' id='msgDiv'>" + close + "<strong>Alerta! </strong>" + message + "</div>"
            break;
        case "error":
            msg = "<div class='alert alert-danger' id='msgDiv'>" + close + "<strong>Error! </strong>" + message + "</div>"
            break;
    }
    fn_setMsj(container, msg);
}
function genDataTable(table) {
    var gentable = $("#" + table).DataTable({
        "scrollX": true
    });
    return gentable;
}
function llamadaAJAX(url, data, onSuccess, onError) {
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: "application/json;",
        dataType: "json",
        success: onSuccess,
        error: onError
    });
}
function llamadaAJAXSync(url, data, onSuccess, onError) {
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: "application/json;",
        dataType: "json",
        async: false,
        success: onSuccess,
        error: onError
    });
}
function llamadaAJAX_Complete(url, data, onSuccess, onError, onComplete) {
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: "application/json;",
        dataType: "json",
        async: true,
        success: onSuccess,
        complete: onComplete,
        error: onError
    });
}
function genDataTableHorizontalScroll(table) {
    var gentable = $("#" + table).DataTable({
        "scrollX": true,
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }
    });
    return gentable;
}
