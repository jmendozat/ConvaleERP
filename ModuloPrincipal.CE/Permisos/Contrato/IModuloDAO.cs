﻿using ModuloPrincipal.CD.Base.Contrato;
using ModuloPrincipal.CD.Permisos.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Permisos.Contrato
{
    public interface IModuloDAO : ICrudDAO<Modulo>
    {
    }
}
