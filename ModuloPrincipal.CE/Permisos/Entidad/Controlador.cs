﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Permisos.Entidad
{
    public class Controlador : Generico
    {
        public Modulo Modulo { get; set; }
        public List<LineaMetodoController> ListaMetodoController { get; set; }

        public void AgregarLineaMetodoController(LineaMetodoController lineaMetodoController) {
            ExisteLineaMetodo(lineaMetodoController);
            lineaMetodoController.GenerarURL(Modulo);
            ListaMetodoController.Add(lineaMetodoController);     
        }

        private void ExisteLineaMetodo(LineaMetodoController lineaMetodoController) {
            foreach (LineaMetodoController metodoController in ListaMetodoController) {
                if (metodoController.Metodo.Id == lineaMetodoController.Metodo.Id)
                    throw new Exception("Ya existe un metodo en la linea");
                }
        }

    }
}
