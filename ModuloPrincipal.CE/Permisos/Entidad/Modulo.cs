﻿using ModuloPrincipal.CD.Permisos.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Permisos.Entidad
{
    public class Modulo : Generico
    {
        public List<Controlador> ListaControlador { get; set; }

    }
}
