﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Permisos.Entidad
{
    public class LineaMetodoController
    {
        public string GetUrl { get; set; }
        public Metodo Metodo { get; set; }

        public void GenerarURL(Modulo modulo) {
            GetUrl = modulo.Nombre + "/" + Metodo.Nombre;
        }

    }
}
