﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Permisos.Entidad
{
    public abstract class Generico
    {
        public int Id  { get; set; }
        public string Nombre{ get; set; }
        public int Estado { get; set; }
        public bool IsActivo { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaUltimaModificacion { get; set; }
    }
}
