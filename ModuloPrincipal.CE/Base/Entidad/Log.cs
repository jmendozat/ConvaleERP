﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Base.Entidad
{
    public abstract class Log
    {
        public int Id { get; set; }
        public string Mensaje { get; set; }
        public string Detalle { get; set; }
        public string Fecha { get; set; }
        public string IPEquipo { get; set; }

    }
}
