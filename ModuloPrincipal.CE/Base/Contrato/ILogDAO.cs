﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Base.Contrato
{
    public interface ILogDAO<T>
    {
        bool Insertar(T log);
    }
}
