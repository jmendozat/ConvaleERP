﻿using ModuloPrincipal.CD.Base.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Base.Contrato
{
    public interface IAuditoriaDAO : ILogDAO<Auditoria>
    {

    }
}
