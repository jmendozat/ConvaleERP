﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Base.Contrato
{
    public interface ICoreDAO<T>
    {
        bool Insertar(T entidad);
        bool Actualizar(T entidad);
        T Buscar(int ID);

    }
}
