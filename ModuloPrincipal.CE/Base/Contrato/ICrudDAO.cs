﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuloPrincipal.CD.Base.Contrato
{
    public interface ICrudDAO<T>
    {
        bool Crear(T entidad);
        bool Editar(T entidad);
        bool Eliminar(T entidad);
        List<T> Listar();


    }
}
