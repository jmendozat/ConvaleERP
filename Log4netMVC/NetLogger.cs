﻿using log4net;
using ModuloPrincipal.CA.Servicios.Base;
using ModuloPrincipal.CD.Base.Entidad;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Log4netMVC
{
    public abstract class NetLogger
    {
        private static ILog log = LogManager.GetLogger(ConfigurationManager.AppSettings["getLogger"].ToString());

        public static string FormatoExcepciones(Exception ex)
        {
            return string.Format("[Error : {0}] [Excepción : {1}] [Mensaje : {2}] [Fuente : {3}]",
                string.IsNullOrEmpty(ex.Source) ? "Hubo un error no reconocido." : ex.Source.Trim(),
                string.IsNullOrEmpty(ex.GetType().Name) ? "Hubo un error sin nombre excepción." : ex.GetType().Name.Trim(),
                string.IsNullOrEmpty(ex.Message) ? "Hubo un error sin mensaje." : ex.Message.Trim(),
                string.IsNullOrEmpty(ex.StackTrace) ? "Hubo un error sin fuente de error." : ex.StackTrace.Trim().Replace(System.Environment.NewLine, ""));
        }
        public static void EscribirLog(NivelLog nivel, string msj, string detalle = "")
        {
            try
            {
               
                switch (nivel)
                {
                    case NivelLog.Aplicacion:
                        log.Error("[IP:" + GetIP() + "] " + msj);
                        break;
                    case NivelLog.Auditoria:
                        GestionarAuditoriaServicio.Instance.Insertar(new Auditoria
                        {
                            IPEquipo = GetIP(),
                            Mensaje = msj,
                            Detalle = detalle,
                        });
                        break;
                    case NivelLog.Seguridad:
                        GestionarSeguridadServicio.Instance.Insertar(new Seguridad
                        {
                            IPEquipo = GetIP(),
                            Mensaje = msj,
                            Detalle = detalle,
                        });
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error("[IP:" + GetIP() + "] " + FormatoExcepciones(ex));
            }

        }
        public enum NivelLog
        {
            Aplicacion = 1,
            Seguridad = 2,
            Auditoria = 3
        };

        public static string FormatoError(string msj)
        {
            return "[Mensaje: " + msj + "]";
        }
        private static string GetIP()
        {
            string ans = "";
            try
            {
                System.Web.HttpContext context = System.Web.HttpContext.Current;
                string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipAddress))
                {
                    string[] addresses = ipAddress.Split(',');
                    if (addresses.Length != 0)
                    {
                        return addresses[0];
                    }
                }
                ans = context.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch (Exception)
            {
                ans = "";
            }

            return ans;
        }

    }
}
