﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.IO;
using System.Configuration;
using System.Net.Mime;
using Excepciones;

namespace Utilitarios.Emails
{
    public class Email
    {
        #region AtributosPublicos
        public string From { get; set; }
        public string DisplayName { get; set; }
        public string To { get; set; }
        public string CopyTo { get; set; }
        public string BlindCarbonCopy { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public bool IsBodyHtml { get; set; }
        public string AttachmentFileNames { get; set; }
        public string UserPassword { get; set; }
        #endregion 
        #region AtributosPrivados
        private MailMessage Message { get; set; }
        private List<ImageEmail> ListaImagenes = new List<ImageEmail>();
        private Configuration settings;
        #endregion 

        public Email(string From, string DisplayName, string To, string CopyTo, string BlindCarbonCopy, string Subject, string Body, bool IsBodyHtml, string AttachmentFileNames, string UserPassword)
        {
            this.Message = new MailMessage();
            this.settings = new Configuration();
            this.From = From;
            this.DisplayName = From;
            this.To = To;
            this.CopyTo = CopyTo;
            this.BlindCarbonCopy = BlindCarbonCopy;
            this.Subject = Subject;
            this.Body = Body;
            this.IsBodyHtml = IsBodyHtml;
            this.AttachmentFileNames = AttachmentFileNames;
            this.UserPassword = UserPassword;
        }

        public void SendEmail()
        {
            try
            {
                CheckInputParameters();
                ConfigSender();
                ConfigAddressee();
                ConfigBody();
                ConfigAttachments();
                SendMessage();
            }
            catch (Exception)
            {
                throw;
            }
        
        }

        private void CheckInputParameters(){
            if (string.IsNullOrEmpty(From)) throw ExceptionEmail.CrearErrorDireccionRemitente();
            if (string.IsNullOrEmpty(To)) throw ExceptionEmail.CrearErrorDireccionDestinatario();
            if (string.IsNullOrEmpty(Body)) throw ExceptionEmail.CrearErrorCuerpoMensaje();
        }

        private void ConfigSender()
        {
            ValidMailAddress(From);
            if (string.IsNullOrEmpty(DisplayName))
                Message.From = new MailAddress(From);
            else
                Message.From = new MailAddress(From, DisplayName);
            Message.Priority = MailPriority.High;
        }

        private void ConfigAddressee()
        {
            var toList = GetList(To);
            var copyToList = GetList(CopyTo);
            var blindCarbonCopyToList = GetList(BlindCarbonCopy);

            ValidEmailAddresses(new List<string>[] { toList, copyToList, blindCarbonCopyToList });

            toList.ForEach(item => Message.To.Add(item.Length == 6 ? item + "@credito.bcp.com.pe" : item));
            copyToList.ForEach(item => Message.CC.Add(item.Length == 6 ? item + "@credito.bcp.com.pe" : item));
            blindCarbonCopyToList.ForEach(item => Message.Bcc.Add(item.Length == 6 ? item + "@credito.bcp.com.pe" : item));
        }

        private List<string> GetList(string separatedValues)
        {
            if (string.IsNullOrEmpty(separatedValues)) return new List<string>();
            return separatedValues.Split(new char[] { ',', ';' }, options: StringSplitOptions.RemoveEmptyEntries).ToList<string>();
        }

        #region Validacion de Email
        private void ValidEmailAddresses(List<string>[] array_list_addresses)
        {
            foreach (var list_addresses in array_list_addresses)
            {
                ValidEmailAddresses(list_addresses);
            }
        }

        private void ValidEmailAddresses(List<string> addresses)
        {
            foreach (var address in addresses)
            {
                ValidMailAddress(address);
            }
        }

        private void ValidMailAddress(string emailAddress)
        {
            try
            {
                new MailAddress(emailAddress.Length == 6 ? emailAddress + "@credito.bcp.com.pe" : emailAddress);
            }
            catch (FormatException)
            {
                throw ExceptionEmail.CrearErrorDireccionEmail(emailAddress);
            }
        }
        #endregion 

        private void ConfigBody()
        {
            try
            {
                Message.Subject = Subject;
                Message.Body = Body;
                Message.IsBodyHtml = IsBodyHtml;
                AddImagesCorreo();
            }
            catch (Exception)
            {
                throw;
            }
           
        }


        #region MetodosImagenesCorreo

        private void AddImagesCorreo() {
            try
            {
                List<LinkedResource> listaImageLink = new List<LinkedResource>(); ;
                foreach (ImageEmail imageEmail in ListaImagenes)
                {
                    if (RequiereImage(imageEmail.NombreParametro))
                    {
                        ValidImage(imageEmail);
                        Message.Body = Message.Body.Replace(imageEmail.NombreParametro, imageEmail.Html);
                        Message.Body = Message.Body.Replace(Environment.NewLine, "<br />");
                        listaImageLink.Add(CreateImageLinkedResource(imageEmail));                     
                    }
                }
                var htmlView = AlternateView.CreateAlternateViewFromString(Message.Body, Encoding.UTF8, Configuration.CONST_BODY_CONTENT_TYPE);

                foreach(LinkedResource imageLink in listaImageLink){
                     htmlView.LinkedResources.Add(imageLink);
                }

                Message.AlternateViews.Add(htmlView);
            }
            catch (Exception)
            {
                throw ExceptionEmail.CrearErrorAddImgMensaje();
            }

        }

        private bool RequiereImage(string nombreParametro)
        {
            return Message.Body.Contains(nombreParametro);
        }

        public void AgregarImageEmail(ImageEmail imageEmail) {
            ListaImagenes.Add(imageEmail);
        }

        private void ValidImage(ImageEmail imageEmail)
        {
            if (!File.Exists(AppDomain.CurrentDomain.BaseDirectory + "src\\images\\" + ConfigurationManager.AppSettings[imageEmail.Key].ToString()))
                throw ExceptionEmail.CrearErrorImagenNoDisponible(imageEmail.NombreParametro);            
        }

        private LinkedResource CreateImageLinkedResource(ImageEmail imageEmail)
        {
            var imageLink = new LinkedResource(AppDomain.CurrentDomain.BaseDirectory + "src\\images\\" + ConfigurationManager.AppSettings[imageEmail.Key].ToString())
            {
                ContentId = imageEmail.Cid,
                TransferEncoding = TransferEncoding.Base64
            };
            return imageLink;
        }
        #endregion

        private void ConfigAttachments()
        {
            if (RequiereSendAttachments())
            {
                AddAttachments();
            }
        }

        #region MetodosArchivosAjuntos
        private bool RequiereSendAttachments()
        {
            return !string.IsNullOrEmpty(AttachmentFileNames);
        }

        private void AddAttachments()
        {
            var attachments = GetList(AttachmentFileNames);
            foreach (var attachment in attachments)
            {
                if (!File.Exists(attachment))
                    throw ExceptionEmail.CrearErrorAdjuntarArchivo(attachment);               
                Attachment emailAttachment = new Attachment(attachment);
                Message.Attachments.Add(emailAttachment);
            }
        }
        #endregion 

        private void SendMessage()
        {
            try
            {
                var smtpClient = BuildSmtpClient();

                if (settings.EnviarNotificaciones)
                    smtpClient.Send(Message);

            }
            catch (SmtpFailedRecipientException ex) { 
                //NetLogger.EscribirLog(NetLogger.NivelLog.Aplicacion, NetLogger.FormatoExcepciones(ex)); /*log.Error(ex.Message);*/
                throw ExceptionEmail.CrearErrorEnvioMsmDestinatario();
            }
            catch (SmtpException ex) { 
                //NetLogger.EscribirLog(NetLogger.NivelLog.Aplicacion, NetLogger.FormatoExcepciones(ex));/*log.Error(ex.Message);*/
                throw ExceptionEmail.CrearErrorConexionServidor();
            }
        }

        private SmtpClient BuildSmtpClient()
        {
            var smtpClient = new SmtpClient();
            smtpClient.Host = settings.SMTP_Server;
            smtpClient.Port = settings.SMTP_Port;
            smtpClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            smtpClient.EnableSsl = settings.SMTP_SSL;
            return smtpClient;
        }
    }
}
