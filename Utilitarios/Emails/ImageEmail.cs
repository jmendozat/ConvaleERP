﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilitarios.Emails
{
    public class ImageEmail
    {
        public string NombreParametro { get; set; }
        public string Html { get; set; }
        public string Key { get; set; }
        public string Cid { get; set; }

        public ImageEmail(string NombreParametro, string Html, string Key, string Cid)
        {
            this.NombreParametro = NombreParametro;
            this.Html = Html;
            this.Key = Key;
            this.Cid = Cid;
        }

    }
}
