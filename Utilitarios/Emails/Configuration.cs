﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace Utilitarios.Emails
{
    public class Configuration
    {
        public const string CONST_BODY_CONTENT_TYPE = "text/html";

        public string SMTP_Server {get{return Get<string>("SMTP_Server");}}
        public int SMTP_Port { get { return GetInt("SMTP_Port"); } }
        public bool SMTP_SSL { get { return GetBoolean("SMTP_SSL"); } }
        public bool EnviarNotificaciones { get { return GetBoolean("EnviarNotificaciones"); } }

        private T Get<T>(string settingKey)
        {
            object setting = ConfigurationManager.AppSettings.Get(settingKey);
            if (setting != null)
                return (T)setting;

            return default(T);
        }
        private int GetInt(string settingKey)
        {
            return int.Parse(ConfigurationManager.AppSettings.Get(settingKey));
        }
        private bool GetBoolean(string settingKey)
        {
            return bool.Parse(ConfigurationManager.AppSettings.Get(settingKey));
        }
    }
}
