﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilitarios.Utils
{
    public abstract class Parametros
    {
        public const string TIPO_EXITO = "exito";
        public const string TIPO_INFO = "info";
        public const string TIPO_ALERTA = "alerta";
        public const string TIPO_ERROR = "error";
        
    }
}
