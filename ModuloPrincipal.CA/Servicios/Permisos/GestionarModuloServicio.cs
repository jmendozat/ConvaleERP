﻿using ModuloPrincipal.CD.Permisos.Contrato;
using ModuloPrincipal.CP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuloPrincipal.CD.Permisos.Entidad;
using ModuloPrincipal.CP.Permisos;

namespace ModuloPrincipal.CA.Servicios.Permisos
{
   public class GestionarModuloServicio
    {
        #region Singleton    
        private static GestionarModuloServicio instance = null;
        public static GestionarModuloServicio Instance
        {
            get
            {
                if (instance == null)
                    instance = new GestionarModuloServicio();
                return instance;
            }
        }
        #endregion
        private GestorSqlServer GestorSqlServer;
        private IModuloDAO ModuloDAO;
        public GestionarModuloServicio()
        {
            GestorSqlServer = new ConexionSqlServer();
            ModuloDAO = new ModuloDAO(GestorSqlServer);
        }

        public bool Crear(Modulo entidad)
        {
            throw new NotImplementedException();
        }

        public bool Editar(Modulo entidad)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(Modulo entidad)
        {
            throw new NotImplementedException();
        }

        public List<Modulo> Listar()
        {
            try
            {
                GestorSqlServer.AbrirConexion();
                List<Modulo> ListaModulo = ModuloDAO.Listar();
                GestorSqlServer.CerrarConexion();
                return ListaModulo;
            }
            catch (Exception)
            {
                GestorSqlServer.CerrarConexion();
                throw;
            }
        }
    }
}
