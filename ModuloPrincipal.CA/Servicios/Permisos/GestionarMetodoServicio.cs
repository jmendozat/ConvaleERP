﻿using ModuloPrincipal.CD.Permisos.Contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuloPrincipal.CD.Permisos.Entidad;
using ModuloPrincipal.CP;
using ModuloPrincipal.CP.Permisos;

namespace ModuloPrincipal.CA.Servicios.Permisos
{
    public class GestionarMetodoServicio
    {
        #region Singleton    
        private static GestionarMetodoServicio instance = null;
        public static GestionarMetodoServicio Instance
        {
            get
            {
                if (instance == null)
                    instance = new GestionarMetodoServicio();
                return instance;
            }
        }
        #endregion
        private GestorSqlServer GestorSqlServer;
        private IMetodoDAO MetodoDAO;
        public GestionarMetodoServicio() {
            GestorSqlServer = new ConexionSqlServer();
            MetodoDAO = new MetodoDAO(GestorSqlServer);
        }
    
        public bool Crear(Metodo entidad)
        {
            try
            {
                GestorSqlServer.AbrirConexion();
                GestorSqlServer.IniciarTransaccion("Transact - Guardar Metodo");
                bool resultado = MetodoDAO.Crear(entidad);
                GestorSqlServer.TerminarTransaccion();
                return resultado;
            }
            catch (Exception)
            {
                GestorSqlServer.CancelarTransaccion();
                throw;
            }
        }

        public bool Editar(Metodo entidad)
        {
            throw new NotImplementedException();
        }

        public bool Eliminar(Metodo entidad)
        {
            throw new NotImplementedException();
        }

        public List<Metodo> Listar()
        {
            try
            {
                GestorSqlServer.AbrirConexion();
                List<Metodo> listaMetodo = MetodoDAO.Listar();
                GestorSqlServer.CerrarConexion();
                return listaMetodo;
            }
            catch (Exception)
            {
                GestorSqlServer.CerrarConexion();
                throw;
            }
        }
    }
}
