﻿using ModuloPrincipal.CD.Base.Contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuloPrincipal.CD.Base.Entidad;
using ModuloPrincipal.CP;
using ModuloPrincipal.CP.Base;
using ModuloPrincipal.CP.FabricaDAO;

namespace ModuloPrincipal.CA.Servicios.Base
{
    public class GestionarAuditoriaServicio
    {
        #region Singleton    
        private static GestionarAuditoriaServicio instance = null;
        public static GestionarAuditoriaServicio Instance
        {
            get
            {
                if (instance == null)
                    instance = new GestionarAuditoriaServicio();
                return instance;
            }
        }
        #endregion

        private GestorSqlServer GestorSqlServer;
        private IAuditoriaDAO AuditoriaDAO;
        public GestionarAuditoriaServicio()
        {     
            FabricaAbstractaDAO fabricaDAO = FabricaAbstractaDAO.Instancia();
            GestorSqlServer = fabricaDAO.CrearGestorSqlServer();
            AuditoriaDAO = fabricaDAO.CrearAuditoriaDAO(GestorSqlServer);
        }
        public bool Insertar(Auditoria log)
        {
            try
            {
                GestorSqlServer.AbrirConexion();
                bool resultado = AuditoriaDAO.Insertar(log);
                GestorSqlServer.CerrarConexion();
                return resultado;
            }
            catch (Exception)
            {
                GestorSqlServer.CerrarConexion();
                throw;
            }
        }
    }
}
