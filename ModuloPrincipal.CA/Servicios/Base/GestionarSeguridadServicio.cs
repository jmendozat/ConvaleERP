﻿using ModuloPrincipal.CD.Base.Contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuloPrincipal.CD.Base.Entidad;
using ModuloPrincipal.CP;
using ModuloPrincipal.CP.Base;

namespace ModuloPrincipal.CA.Servicios.Base
{
    public class GestionarSeguridadServicio
    {
        #region Singleton    
        private static GestionarSeguridadServicio instance = null;
        public static GestionarSeguridadServicio Instance
        {
            get
            {
                if (instance == null)
                    instance = new GestionarSeguridadServicio();
                return instance;
            }
        }
        #endregion
        private GestorSqlServer GestorSqlServer;
        private ISeguridadDAO SeguridadDAO;
        public GestionarSeguridadServicio()
        {
            GestorSqlServer = new ConexionSqlServer();
            SeguridadDAO = new SeguridadDAO(GestorSqlServer);
        }

        public bool Insertar(Seguridad log)
        {
            try
            {
                GestorSqlServer.AbrirConexion();
                bool resultado = SeguridadDAO.Insertar(log);
                GestorSqlServer.CerrarConexion();
                return resultado;
            }
            catch (Exception)
            {
                GestorSqlServer.CerrarConexion();
                throw;
            }
        }
      
    }
}
